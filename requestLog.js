var CircularLog = require('./CircularLog.js');
var myLog = new CircularLog({
  host: 'localhost',
  user: 'dbuser',
  password: 'password',
  database: 'rpibpr',
  table: 'request_log',
  rows: 16
});

// Create log table if it does not exist
myLog.createTable();

module.exports = myLog;