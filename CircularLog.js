var DEFAULT_ROWS = 4;

var mysql = require('mysql');

// A fixed database table schema is assumed as follows:
//   CREATE TABLE circular_log_table (
//       log_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//       row_id INTEGER UNSIGNED NOT NULL UNIQUE KEY,
//       timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
//       payload VARCHAR(255),
//       INDEX (timestamp)
//   );

// Constructor
function CircularLog(config) {
  this.config = config;
  this.config.rows = this.config.rows || DEFAULT_ROWS;
  
};

// Properties and methods
CircularLog.prototype.createTable = function() {
  if (!this.config.table || !this.config.database) {
    return;
  }
  
  var dbConn = mysql.createConnection({
    host: this.config.host,
    user: this.config.user,
    password: this.config.password,
    database: this.config.database
  });
  
  dbConn.connect();
  
  // Create the table
  var sqlCreate = 'CREATE TABLE IF NOT EXISTS ' + this.config.table + ' (' + '\r' +
                  'log_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,' + '\r' +
                  'row_id INTEGER UNSIGNED NOT NULL UNIQUE KEY,' + '\r' +
                  'timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,' + '\r' +
                  'payload VARCHAR(255),' + '\r' +
                  'INDEX (timestamp));';
  dbConn.query(sqlCreate, function(err, res) {
    if (err) {
      console.log('Error creating table: ' + err);
    }
  });

  dbConn.end(function(err) {
    if (err) {
      console.log('Database disconnection error: ' + err);
    }
  });
};

CircularLog.prototype.append = function(payload, callback) {
  // Note: every method called on dbConn is queued and executed in sequence
  var dbConn = mysql.createConnection({
    host: this.config.host,
    user: this.config.user,
    password: this.config.password,
    database: this.config.database
  });
  
  dbConn.connect();
  var sqlAppend = 'REPLACE INTO ' + this.config.table + '\r' +
                  'SET row_id = (SELECT COALESCE(MAX(log_id), 0) % ' + this.config.rows + '+ 1' + '\r' +
                  'FROM ' + this.config.table + ' AS t),' + '\r' +
                  'payload = \'' + payload + '\';';
  dbConn.query(sqlAppend, function(err, res) {
    if (err) {
      console.log('Database query error: ' + err);
    }
    if (callback) callback();
  });
  dbConn.end(function(err) {
    if (err) {
      console.log('Database disconnection error: ' + err);
    }
  });
};

CircularLog.prototype.getTable = function(callback) {
  var dbConn = mysql.createConnection({
    host: this.config.host,
    user: this.config.user,
    password: this.config.password,
    database: this.config.database
  });
  
  dbConn.connect();
  dbConn.query('SELECT timestamp, payload FROM ' + this.config.table + ';', function(err, res) {
    if (err) {
      if (callback) callback(err);
      console.log('Database query error: ' + err);
    }
    if (callback) callback(err, res);
  });
  dbConn.end(function(err) {
    if (err) {
      if (callback) callback(err);
      console.log('Database disconnection error: ' + err);
    }
  });
};

// node.js module export
module.exports = CircularLog;
