var express = require('express');
var request = require('request');
var routes = require('./routes.js');
var logdb = require('./requestLog.js');

var app = express();
var port = process.env.PORT || 8080;

var publicPort = 60080;
var publicIP = '';

// Get and update current public IP to XSS
function checkIP(callback) {
  request.get({url: 'http://checkip.amazonaws.com'}
    , function(err, resp, body) {
    if (err) {
      console.log(err);
      return;
    }
    var currentIP = body.trim();
    console.log('My public IP is: ' + currentIP);
    if (callback) callback(currentIP);
  });
}

function updateIP(callback) {
  checkIP(function(currentIP) {
    publicIP = currentIP;
	console.log(currentIP);
    // Update the IP to XSS
    var setaddressBody = {address: 'http://' + publicIP, port: publicPort};
    request.post({url: 'http://edcxssbpr.herokuapp.com/setaddress', 
        body: JSON.stringify(setaddressBody),
		json: true
      }, function(err, resp, recvbody) {
	  console.log(setaddressBody);
      if (err) {
        console.log(err);
        return;
      }
      console.log('XSS: ' + recvbody);
      if (callback) callback(recvbody);
    });
  });
}

// Update the public IP to XSS
// updateIP();

// Set up logging route
app.all('*', function(req, res, next) {
  // Don't log the /log route
  if (req.path !== '/log') {
    logdb.append('Host ' + req.ip + ' requests ' + req.method +
                 ' ' + req.path + ' UUID: ' + req.query.uuid);
  }
  return next();
});

// Set up additional route to update IP
app.get('/updateip', function(req, res) {
  updateIP(function(body) {
    res.send(body);
  });
});

// Check current IP
app.get('/checkip', function(req, res) {
  checkIP(function(currentIP) {
    res.send('Current public IP is: ' + currentIP);
  });
});

// Set up the routes
routes(app);


app.listen(port, function(err) {
  console.log('Listening on port ' + port);
});
