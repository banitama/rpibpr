var logdb = require('./requestLog.js');
var mysql = require('mysql');

var pollPeriod = 100;
var pollTimeout = 5000;

// Database schema
//
// 'request' table:
//   CREATE TABLE request(
//     idx BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//     uuid CHAR(64),
//     timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
//     operation VARCHAR(256),
//     param VARCHAR(256),
//     isread BOOLEAN);
//
// 'response' table:
//   CREATE TABLE response(
//     idx BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//     uuid CHAR(64),
//     timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
//     data VARCHAR(256));
 
module.exports = function(app) {
  app.get('/', function(req, res) {
    res.type('text/plain');
    res.send('You have reached the root of edcxssbpr -- UUID: ' + req.query.uuid);
  });

  app.post('/', function(req, res) {
    res.type('text/plain');
    res.send('You have reached the root of edcxssbpr -- UUID: ' + req.query.uuid);
  });
  
  app.get('/tabungan/checksaldo.json', function(req, res) {
    console.log(req.query);

    // Insert into request table in the database
    var dbConn = mysql.createConnection({
      host: 'localhost',
      user: 'dbuser',
      password: 'password',
      database: 'rpibpr'
    });
    
    dbConn.connect();
    var uuid = req.query.uuid;
    delete req.query['__proto__'];
    delete req.query.uuid;
    
    var sqlAppend = 'INSERT INTO request(uuid, operation, param, isread)\r' +
                    'VALUES (' +
                    "'" + uuid + "'" + ',' +
                    "'" + req.path + "'" + ',' +
                    "'" + JSON.stringify(req.query) + "'" + ',' +
                    'false' +
                    ');';
    dbConn.query(sqlAppend, function(err, res) {
      if (err) {
        console.log('Database query error: ' + err);
      }
    });
    dbConn.end(function(err) {
      if (err) {
        console.log('Database disconnection error: ' + err);
      }
    });
    delete dbConn;

    // Poll for the response in the response table
    var pollCounter = 0;
    var pollingLoop = function() {
      pollCounter++;
      if (pollCounter > (pollTimeout/pollPeriod)) {
        res.json({
          "status": "timeout",
          "error_code": -1
        });
        return;
      }
      
      var dbConn = mysql.createConnection({
        host: 'localhost',
        user: 'dbuser',
        password: 'password',
        database: 'rpibpr'
      });
      
      dbConn.connect();
      var sqlSelect = 'SELECT * FROM response\r' +
                      'WHERE uuid=' + "'" + uuid + "'" +
                      ';';
      dbConn.query(sqlSelect, function(err, sqlres) {
        if (err) {
          console.log('Database query error: ' + err);
        }
        console.log(sqlres);
        if (sqlres.length == 0) {
          setTimeout(pollingLoop, pollPeriod);
        } else {
          res.json({
            "status": "dummy_success",
            "error_code": 0,
            "saldo": "15000000"
          });
          // res.json(sqlres);
        }
      });
      
      dbConn.end(function(err) {
        if (err) {
          console.log('Database disconnection error: ' + err);
        }
      });
      delete dbConn;
    }
    
    pollingLoop();
  });

  app.post('/tabungan/setoran.json', function(req, res) {
    console.log(req.query);
    
    // Insert into request table in the database
    var dbConn = mysql.createConnection({
      host: 'localhost',
      user: 'dbuser',
      password: 'password',
      database: 'rpibpr'
    });
    
    dbConn.connect();
    var uuid = req.query.uuid;
    delete req.query['__proto__'];
    delete req.query.uuid;
    
    var sqlAppend = 'INSERT INTO request(uuid, operation, param, isread)\r' +
                    'VALUES (' +
                    "'" + uuid + "'" + ',' +
                    "'" + req.path + "'" + ',' +
                    "'" + JSON.stringify(req.query) + "'" + ',' +
                    'false' +
                    ');';
    dbConn.query(sqlAppend, function(err, res) {
      if (err) {
        console.log('Database query error: ' + err);
      }
    });
    dbConn.end(function(err) {
      if (err) {
        console.log('Database disconnection error: ' + err);
      }
    });
    delete dbConn;

    // Poll for the response in the response table
    var pollCounter = 0;
    var pollingLoop = function() {
      pollCounter++;
      if (pollCounter > (pollTimeout/pollPeriod)) {
        res.json({
          "status": "timeout",
          "error_code": -1
        });
        return;
      }
      
      var dbConn = mysql.createConnection({
        host: 'localhost',
        user: 'dbuser',
        password: 'password',
        database: 'rpibpr'
      });
      
      dbConn.connect();
      var sqlSelect = 'SELECT * FROM response\r' +
                      'WHERE uuid=' + "'" + uuid + "'" +
                      ';';
      dbConn.query(sqlSelect, function(err, sqlres) {
        if (err) {
          console.log('Database query error: ' + err);
        }
        console.log(sqlres);
        if (sqlres.length == 0) {
          setTimeout(pollingLoop, pollPeriod);
        } else {
          res.json({
            "status": "not_implemented",
            "error_code": -1
          });
          // res.json(sqlres);
        }
      });
      
      dbConn.end(function(err) {
        if (err) {
          console.log('Database disconnection error: ' + err);
        }
      });
      delete dbConn;
    }
    
    pollingLoop();
  });

  app.post('/tabungan/tarikan.json', function(req, res) {
    console.log(req.query);
    
    // Insert into request table in the database
    var dbConn = mysql.createConnection({
      host: 'localhost',
      user: 'dbuser',
      password: 'password',
      database: 'rpibpr'
    });
    
    dbConn.connect();
    var uuid = req.query.uuid;
    delete req.query['__proto__'];
    delete req.query.uuid;
    
    var sqlAppend = 'INSERT INTO request(uuid, operation, param, isread)\r' +
                    'VALUES (' +
                    "'" + uuid + "'" + ',' +
                    "'" + req.path + "'" + ',' +
                    "'" + JSON.stringify(req.query) + "'" + ',' +
                    'false' +
                    ');';
    dbConn.query(sqlAppend, function(err, res) {
      if (err) {
        console.log('Database query error: ' + err);
      }
    });
    dbConn.end(function(err) {
      if (err) {
        console.log('Database disconnection error: ' + err);
      }
    });
    delete dbConn;

    // Poll for the response in the response table
    var pollCounter = 0;
    var pollingLoop = function() {
      pollCounter++;
      if (pollCounter > (pollTimeout/pollPeriod)) {
        res.json({
          "status": "timeout",
          "error_code": -1
        });
        return;
      }
      
      var dbConn = mysql.createConnection({
        host: 'localhost',
        user: 'dbuser',
        password: 'password',
        database: 'rpibpr'
      });
      
      dbConn.connect();
      var sqlSelect = 'SELECT * FROM response\r' +
                      'WHERE uuid=' + "'" + uuid + "'" +
                      ';';
      dbConn.query(sqlSelect, function(err, sqlres) {
        if (err) {
          console.log('Database query error: ' + err);
        }
        console.log(sqlres);
        if (sqlres.length == 0) {
          setTimeout(pollingLoop, pollPeriod);
        } else {
          res.json({
            "status": "not_implemented",
            "error_code": -1
          });
          // res.json(sqlres);
        }
      });
      
      dbConn.end(function(err) {
        if (err) {
          console.log('Database disconnection error: ' + err);
        }
      });
      delete dbConn;
    }
    
    pollingLoop();
  });

  app.get('/kredit/baki.json', function(req, res) {
    console.log(req.query);
    
    // Insert into request table in the database
    var dbConn = mysql.createConnection({
      host: 'localhost',
      user: 'dbuser',
      password: 'password',
      database: 'rpibpr'
    });
    
    dbConn.connect();
    var uuid = req.query.uuid;
    delete req.query['__proto__'];
    delete req.query.uuid;
    
    var sqlAppend = 'INSERT INTO request(uuid, operation, param, isread)\r' +
                    'VALUES (' +
                    "'" + uuid + "'" + ',' +
                    "'" + req.path + "'" + ',' +
                    "'" + JSON.stringify(req.query) + "'" + ',' +
                    'false' +
                    ');';
    dbConn.query(sqlAppend, function(err, res) {
      if (err) {
        console.log('Database query error: ' + err);
      }
    });
    dbConn.end(function(err) {
      if (err) {
        console.log('Database disconnection error: ' + err);
      }
    });
    delete dbConn;

    // Poll for the response in the response table
    var pollCounter = 0;
    var pollingLoop = function() {
      pollCounter++;
      if (pollCounter > (pollTimeout/pollPeriod)) {
        res.json({
          "status": "timeout",
          "error_code": -1
        });
        return;
      }
      
      var dbConn = mysql.createConnection({
        host: 'localhost',
        user: 'dbuser',
        password: 'password',
        database: 'rpibpr'
      });
      
      dbConn.connect();
      var sqlSelect = 'SELECT * FROM response\r' +
                      'WHERE uuid=' + "'" + uuid + "'" +
                      ';';
      dbConn.query(sqlSelect, function(err, sqlres) {
        if (err) {
          console.log('Database query error: ' + err);
        }
        console.log(sqlres);
        if (sqlres.length == 0) {
          setTimeout(pollingLoop, pollPeriod);
        } else {
          res.json({
            "status": "not_implemented",
            "error_code": -1
          });
          // res.json(sqlres);
        }
      });
      
      dbConn.end(function(err) {
        if (err) {
          console.log('Database disconnection error: ' + err);
        }
      });
      delete dbConn;
    }
    
    pollingLoop();
  });

  app.get('/kredit/tunggakan.json', function(req, res) {
    console.log(req.query);
    
    // Insert into request table in the database
    var dbConn = mysql.createConnection({
      host: 'localhost',
      user: 'dbuser',
      password: 'password',
      database: 'rpibpr'
    });
    
    dbConn.connect();
    var uuid = req.query.uuid;
    delete req.query['__proto__'];
    delete req.query.uuid;
    
    var sqlAppend = 'INSERT INTO request(uuid, operation, param, isread)\r' +
                    'VALUES (' +
                    "'" + uuid + "'" + ',' +
                    "'" + req.path + "'" + ',' +
                    "'" + JSON.stringify(req.query) + "'" + ',' +
                    'false' +
                    ');';
    dbConn.query(sqlAppend, function(err, res) {
      if (err) {
        console.log('Database query error: ' + err);
      }
    });
    dbConn.end(function(err) {
      if (err) {
        console.log('Database disconnection error: ' + err);
      }
    });
    delete dbConn;

    // Poll for the response in the response table
    var pollCounter = 0;
    var pollingLoop = function() {
      pollCounter++;
      if (pollCounter > (pollTimeout/pollPeriod)) {
        res.json({
          "status": "timeout",
          "error_code": -1
        });
        return;
      }
      
      var dbConn = mysql.createConnection({
        host: 'localhost',
        user: 'dbuser',
        password: 'password',
        database: 'rpibpr'
      });
      
      dbConn.connect();
      var sqlSelect = 'SELECT * FROM response\r' +
                      'WHERE uuid=' + "'" + uuid + "'" +
                      ';';
      dbConn.query(sqlSelect, function(err, sqlres) {
        if (err) {
          console.log('Database query error: ' + err);
        }
        console.log(sqlres);
        if (sqlres.length == 0) {
          setTimeout(pollingLoop, pollPeriod);
        } else {
          res.json({
            "status": "not_implemented",
            "error_code": -1
          });
          // res.json(sqlres);
        }
      });
      
      dbConn.end(function(err) {
        if (err) {
          console.log('Database disconnection error: ' + err);
        }
      });
      delete dbConn;
    }
    
    pollingLoop();
  });

  app.post('/kredit/angsuran.json', function(req, res) {
    console.log(req.query);
    
    // Insert into request table in the database
    var dbConn = mysql.createConnection({
      host: 'localhost',
      user: 'dbuser',
      password: 'password',
      database: 'rpibpr'
    });
    
    dbConn.connect();
    var uuid = req.query.uuid;
    delete req.query['__proto__'];
    delete req.query.uuid;
    
    var sqlAppend = 'INSERT INTO request(uuid, operation, param, isread)\r' +
                    'VALUES (' +
                    "'" + uuid + "'" + ',' +
                    "'" + req.path + "'" + ',' +
                    "'" + JSON.stringify(req.query) + "'" + ',' +
                    'false' +
                    ');';
    dbConn.query(sqlAppend, function(err, res) {
      if (err) {
        console.log('Database query error: ' + err);
      }
    });
    dbConn.end(function(err) {
      if (err) {
        console.log('Database disconnection error: ' + err);
      }
    });
    delete dbConn;

    // Poll for the response in the response table
    var pollCounter = 0;
    var pollingLoop = function() {
      pollCounter++;
      if (pollCounter > (pollTimeout/pollPeriod)) {
        res.json({
          "status": "timeout",
          "error_code": -1
        });
        return;
      }
      
      var dbConn = mysql.createConnection({
        host: 'localhost',
        user: 'dbuser',
        password: 'password',
        database: 'rpibpr'
      });
      
      dbConn.connect();
      var sqlSelect = 'SELECT * FROM response\r' +
                      'WHERE uuid=' + "'" + uuid + "'" +
                      ';';
      dbConn.query(sqlSelect, function(err, sqlres) {
        if (err) {
          console.log('Database query error: ' + err);
        }
        console.log(sqlres);
        if (sqlres.length == 0) {
          setTimeout(pollingLoop, pollPeriod);
        } else {
          res.json({
            "status": "not_implemented",
            "error_code": -1
          });
          // res.json(sqlres);
        }
      });
      
      dbConn.end(function(err) {
        if (err) {
          console.log('Database disconnection error: ' + err);
        }
      });
      delete dbConn;
    }
    
    pollingLoop();
  });
  
  // Additional route for getting log
  app.get('/log', function(req, res) {
    logdb.getTable(function(err, data) {
      res.send(data);
    });
  });
  
  app.get('/*', function(req, res) {
    res.type('text/plain');
    res.send('You requested the endpoint ' + req.params.endpoint);
  });
}
